#include <iostream>
#include <conio.h>  
#include "Util.h"
#include "Explosion.h"

using namespace std;

void Explosion::pintar(){

	if ((fexp == 6 ) || (fexp == 5)){
		gotoxy(x,y); printf("%c_%c", 'x','x' );
		gotoxy(x,y+1); printf("_%c_",'x');
		gotoxy(x,y+2); printf("%c_%c", 'x','x' );
		
	}

	if ((fexp == 4 ) || (fexp == 3)){
		gotoxy(x,y); printf("%c_ ", 'x');
		gotoxy(x,y+1); printf("_%c_",'x');
		gotoxy(x,y+2); printf(" _%c", 'x' );
		
	}
	if ((fexp == 2 ) || (fexp == 1)){
		gotoxy(x,y); printf("   " );
		gotoxy(x,y+1); printf("_%c_",'x');
		gotoxy(x,y+2); printf("   " );
	}
	
	if (fexp == 0){
		gotoxy(x,y); printf("   " );
		gotoxy(x,y+1); printf("   ");
		gotoxy(x,y+2); printf("   ");
	}

	fexp--; 
}

void Explosion::borrar(){
	gotoxy(x,y); printf("    ");
	gotoxy(x,y+1); printf("    ");
	gotoxy(x,y+2); printf("    ");
}


Explosion::Explosion(int _x, int _y){
	x = _x;
	y = _y;
	fexp = 6; 
}

void Explosion::mover(){
		borrar();
		pintar(); 
}

bool Explosion::isAlive(){
	return (fexp >= 0); 
}