#include <iostream>
#include "Proyectil.h"
#include "Explosion.h"
using namespace std;



#ifndef  _ENEMIGO_
#define _ENEMIGO_

	class Enemigo{
		int x, y;
		bool n; 
		 
		int fexp; 
		public:
			Enemigo(int _x, int _y);
			void pintar();
			void borrar();
			void mover();
			Proyectil * disparar();
			int Enemigo::esColision(Proyectil p); 
			bool isAlive(); 
			
	};

#endif