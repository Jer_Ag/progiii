#include <iostream>
#include "Proyectil.h"
using namespace std;



#ifndef  _NAVE_
#define _NAVE_

	class Nave{
		int x, y;
		int corazones;
		int vidas;
		public:
			Nave(int _x, int _y, int corazones, int vidas);
			//~Nave();
			void pintar();
			void borrar();
			void mover(char tecla);
			Proyectil * disparar();
			

	};

#endif