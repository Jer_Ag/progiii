#include <iostream>
#include "Proyectil.h"
using namespace std;

#ifndef  _EXPLOSION_
#define  _EXPLOSION_

	class Explosion{
		int x, y;
		int fexp; 
		public:
			Explosion(int _x, int _y);
			void pintar();
			void borrar();
			void mover(); 
			bool isAlive();
	
	};

#endif