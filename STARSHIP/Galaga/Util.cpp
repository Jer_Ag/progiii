#include <iostream>
#include <windows.h>
#include "Util.h"
using namespace std;


#define ARRIBA 72 //H
#define IZQ 75	//K
#define DER 77 //M
#define ABAJO 80 //P


/*
	Posiciona el cursor en las coordenadas x,y en la consola. 
	X: crece de izquierda a derecha.
	Y: crece de arriba a abajo.
*/
void gotoxy(int x , int y)
{
	HANDLE h;
	//recupero el identificador de la consola
	h = GetStdHandle(STD_OUTPUT_HANDLE);
	COORD dwPos;
	dwPos.X = x;
	dwPos.Y = y; //Esta invertida crece hacia abajo
	SetConsoleCursorPosition(h,dwPos);
}



void OcultarCursor(){
HANDLE h;
	h = GetStdHandle(STD_OUTPUT_HANDLE);
	CONSOLE_CURSOR_INFO cci;
	cci.dwSize = 50;		//1..100 estilo del cursos de pantalla el alto del cursos
	cci.bVisible = false;	// FALSE falsO NO SE MUESTRA  TRUE verdaderoSE MUESTRA
	SetConsoleCursorInfo(h,&cci);
}




void pintarLimites(){
	for(int i = 2 ; i<78; i++){
		gotoxy(i,3);printf("%c",205);
		gotoxy(i,33);printf("%c", 205);
	}

	for(int i= 4 ; i<33; i++){
		gotoxy(2,i);printf("%c",186);
		gotoxy(77,i);printf("%c", 186);
	}

	gotoxy(2,3);printf("%c",201);
	gotoxy(2,33);printf("%c",200);
	gotoxy(77,3);printf("%c",187);
	gotoxy(77,33);printf("%c",188);

}