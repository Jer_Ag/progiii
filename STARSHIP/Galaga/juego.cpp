#include <iostream>
#include "Windows.h"
#include "Conio.h"
//
#include "Util.h"
#include "Nave.h"
#include "Enemigo.h"
#include "Explosion.h"

using namespace std;

int main(){

	OcultarCursor(); 
	pintarLimites();
	Nave nave(20,30, 3,3);
	
	
	Enemigo * enemigo = new Enemigo(3,10);
	Enemigo * enemigo2 = new Enemigo(71, 15);
	Enemigo * enemigo3 = new Enemigo(71, 5);
	Enemigo * enemigo4 = new Enemigo(3, 20);
	Proyectil * p = NULL; 
	Explosion * e = NULL;
	Explosion * f = NULL;
	Explosion * g = NULL;
	Explosion * h = NULL;
	bool game_over = false;
	

	nave.pintar(); 
	enemigo->pintar();
	enemigo2->pintar();
	enemigo3->pintar();
	enemigo4->pintar();
	


	while(!game_over){
		// Colisiones
		
		if ((p != NULL) && (enemigo != NULL))
		{
			e = enemigo->esColision(*p);
			f = enemigo2->esColision(*p);
			g = enemigo3->esColision(*p);
			h = enemigo4->esColision(*p);
			if (e != NULL)
			{
				p->dead(); 
			}
			if (f != NULL) {
				p->dead();
			}
			if (g != NULL) {
				p->dead();
			}
			if (h != NULL) {
				p->dead();
			}
		}
		


		//Entrada();
		if (kbhit()){
			char tecla = _getch();
			nave.mover(tecla); 
			if ((tecla == 'q') || (tecla == 'Q'))
				game_over = true; 

			if (tecla =='a')
			{
				if (p == NULL)
					p = nave.disparar(); 
			}
		}

		// Actualizar
		
		if (enemigo != NULL)
		{
			if (enemigo->isAlive()){
				enemigo->mover(); 
			}else{
				enemigo = NULL; 
			}
		}

		if (enemigo2 != NULL) {
			if (enemigo2->isAlive()) {
				enemigo2->mover();
			}
			else {
				enemigo2=NULL;
			}
		}
		
		if (enemigo3 != NULL) {
			if (enemigo3->isAlive()) {
				enemigo3->mover();
			}
			else {
				enemigo3 = NULL;
			}
		}

		if (enemigo4 != NULL) {
			if (enemigo4->isAlive()) {
				enemigo4->mover();
			}
			else {
				enemigo4 = NULL;
			}
		}



		if (p != NULL) 
		{
			p->mover(); 
			if (!(p->isAlive()))
			{
				delete p; 
				p = NULL; 
			}
		}
		
		if (e!= NULL)
		{
			e->mover();
			if (!(e->isAlive()))
			{
				delete e; 
				e = NULL; 
			}
		}
		if (f != NULL) {
			f->mover();
			if (!(f->isAlive())) {
				delete f;
				f = NULL;
			}
		}
		if (g != NULL) {
			g->mover();
			if (!(g->isAlive())) {
				delete g;
				g= NULL;
			}
		}
		if (h != NULL) {
			h->mover();
			if (!(h->isAlive())) {
				delete h;
				h = NULL;
			}
		}
		
		//game_over =  ((e == NULL) && (enemigo == NULL)) ;
		Sleep(30); // Para que no sature el procesado ejecutandose una y otra vez.		
	}
	gotoxy(30,30);
	printf("El juego finalizo\n");
	//system("pause");
	return 0;
}
