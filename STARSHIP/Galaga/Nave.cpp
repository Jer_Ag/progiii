#include <iostream>
#include <conio.h>  
#include "Util.h"
#include "Nave.h"

using namespace std;

void Nave::pintar(){
	gotoxy(x,y); printf("  %c", '*');
	gotoxy(x,y+1); printf(" %c%c%c",'*','*','*');
	gotoxy(x,y+2); printf("%c%c %c%c",'*','*','*','*');
}

void Nave::borrar(){
	gotoxy(x,y); printf("     ");
	gotoxy(x,y+1); printf("     ");
	gotoxy(x,y+2); printf("     ");
}

void Nave::mover(char tecla){
		borrar();
		if (tecla == IZQ && x>3) x--;
		if (tecla == DER && x+6<77) x++;
		if (tecla == ARRIBA && y > 4 ) y--;
		if (tecla == ABAJO && y +3< 33 ) y++;
		pintar(); 
}



Proyectil * Nave::disparar(){

	Proyectil * p = new Proyectil(x + 2,y - 1); 
	return p; 
}

Nave::Nave(int _x, int _y, int _c, int _v){
	x = _x;
	y = _y;
	corazones = _c;
	vidas = _v;
}



