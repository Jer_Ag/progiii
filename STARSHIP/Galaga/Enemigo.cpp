#include <iostream>
#include <conio.h>  
#include "Util.h"
#include "Enemigo.h"

using namespace std;

void Enemigo::pintar(){

		gotoxy(x,y); printf(" %c", '*');
		gotoxy(x,y+1); printf("%c%c%c",'*','*','*');
		gotoxy(x,y+2); printf(" %c", '*');
	
}

void Enemigo::borrar(){
	gotoxy(x,y); printf("      ");
	gotoxy(x,y+1); printf("      ");
	gotoxy(x,y+2); printf("      ");
}


void Enemigo::mover(){
	borrar();
	if (n)
	{
		if (x>3){
			x--;
		}
		else{
			n = false; 
		}
	}else{
		if (x+6<77){
			x++;
		}else{
			n = true; 
		}
	}
	pintar(); 
}


int Enemigo::esColision(Proyectil p)
{
	int xp = p.getX(); 
	int yp = p.getY();
	return NULL;
}

Enemigo::Enemigo(int _x, int _y){
	x = _x;
	y = _y;
	n = false; 
	
	fexp = 3; 
}

bool Enemigo::isAlive(){
	return true; 
}