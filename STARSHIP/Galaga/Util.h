#include <iostream>
using namespace std;


#ifndef _UTIL_
#define _UTIL_

#define ARRIBA 72
#define IZQ 75
#define DER 77
#define ABAJO 80

/*
	Posiciona el cursor en las coordenadas x,y en la consola. 
	X: crece de izquierda a derecha.
	Y: crece de arriba a abajo.
*/

void gotoxy(int x , int y);
void OcultarCursor();
void pintarLimites(); 
#endif