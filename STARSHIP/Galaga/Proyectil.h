#include <iostream>
using namespace std;



#ifndef  _PROYECTIL
#define _PROYECTIL

	class Proyectil{
		int x, y;
		bool live; 
	
		public:

			Proyectil(int _x, int _y);
			~Proyectil(){
				borrar(); 
			//printf("xxxxxxxxxxxxxxxxxxx ");
			};
			int getX(){
				return x; 
			};
			
			int getY(){
				return y; 
			}
			void dead(); 
			void pintar();
			void borrar();
			void mover();
			bool isAlive(); 
	};

#endif