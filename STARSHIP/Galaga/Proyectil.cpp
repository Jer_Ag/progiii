#include <iostream>
#include <conio.h>  
#include "Util.h"
#include "Proyectil.h"

using namespace std;

void Proyectil::pintar(){
	gotoxy(x,y); printf("%c", '*');
}

void Proyectil::borrar(){
	gotoxy(x,y); printf(" ");
}

void Proyectil::mover(){
		borrar();
		//solo se mueve hacia arriba
		if (y > 4 ) 
		{
			y--;		
			pintar(); 
		}
}

void Proyectil::dead(){
	live = false; 
}

bool Proyectil::isAlive(){

	if (live)
		return (y>4); 
	else
		return false; 
}

Proyectil::Proyectil(int _x, int _y){
	x = _x;
	y = _y;
	live = true; 
}


//
//Proyectil::~Proyectil(){
//
//	printf("xxxxxxxxxxxxxxxxxxx ");
//	borrar(); 
//}

